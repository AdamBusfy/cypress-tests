/// <reference types="cypress" />

describe('Redirects', () => {
  
  beforeEach( () => {
    cy.visitChatyUrlAndAcceptCookies();
  })

  it('sell', () => {
    cy.contains('Hľadať').click();
    cy.url()
      .should('include', '/predaj');
    cy.contains('Chaty na predaj');
  });
  
  it('rent', () => {
    cy.contains('Dlhodobý prenájom').click();
    cy.contains('Hľadať').click();
    cy.url()
      .should('include', '/prenajom');
    cy.contains('Chaty na prenájom');
  });

  it('holiday', () => {
    cy.contains('Chaty na dovolenku').click();
    cy.get('.vacation-filter-data > form > .button').click();
    cy.url()
      .should('include', '/dovolenky');
    cy.contains('Chaty a chalupy pre vašu ideálnu dovolenku');
  });
});