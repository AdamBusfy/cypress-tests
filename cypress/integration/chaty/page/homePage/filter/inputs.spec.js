/// <reference types="cypress" />

describe('Inputs', function() {
  
    before( () => {
        cy.visitChatyUrlAndAcceptCookies();
    });
  
    it('type', () => {
        cy.get('[data-value="chaty-chalupy"]')
            .should('not.be.visible');
        cy.get('#obsah > section.filter > div.filter-data.filter-style > form > div:nth-child(1) > div > div.select-lookalike.no-select')
            .click()
            .should('have.class', 'active');
        cy.get('[data-value="chaty-chalupy"]')
            .should('be.visible');

        cy.get('[data-value="chaty-chalupy"]')
            .click();
        
    });

    it('location', () => {
        cy.get(':nth-child(2) > .fancy-selector-wrapper > .location')
        .should('have.value', '')
        .type('Hello World')
        .should('have.value', 'Hello World')
        .clear()
        .should('have.value', '');    
    });

    it('price', () => {
        cy.get('.price-from')
            .should('have.value', '')
            .type('0')
            .should('have.value', '0')
            .clear()
            .should('have.value', '');

        cy.get('.price-to')
            .should('have.value', '')
            .type('5000')
            .should('have.value', '5000')
            .clear()
            .should('have.value', '');
    });
    
    it('advanced-search', () => {
        cy.get('.advanced-search')
            .click();
        cy.get('#obsah > section.filter > div.filter-data.filter-style > form > span')
            .should('have.class', 'active');
        cy.get('.advanced-search')
            .click();
        cy.get('#obsah > section.filter > div.filter-data.filter-style > form > span')
            .should('not.have.class', 'active');
    });

    it('condition', () => {
        cy.get('.advanced-search')
            .click();
        cy.get('#obsah > section.filter > div.filter-data.filter-style > form > span')
            .should('have.class', 'active');
        cy.get('[data-value=povodny-stav]')
            .should('not.be.visible');
        cy.get('#obsah > section.filter > div.filter-data.filter-style > form > div.advanced-search-tab > div:nth-child(1) > div > div.select-lookalike.no-select')
            .click()
            .should('have.class', 'active');
        cy.get('[data-value="povodny-stav"]')
            .should('be.visible')

        cy.get('[data-value="povodny-stav"]')
            .click();
     
        cy.get('.advanced-search')
            .click();
        cy.get('#obsah > section.filter > div.filter-data.filter-style > form > span')
            .should('not.have.class', 'active');
    });

    it('distance', () => {
        cy.get('.advanced-search')
            .click();
        cy.get('#obsah > section.filter > div.filter-data.filter-style > form > span')
            .should('have.class', 'active');
        cy.get('.distance')
            .should('be.disabled');

        cy.get('.advanced-search')
            .click();
        cy.get('#obsah > section.filter > div.filter-data.filter-style > form > span')
            .should('not.have.class', 'active');
    });

    it('distance-area', () => {
        cy.get('.advanced-search')
            .click();
        cy.get('#obsah > section.filter > div.filter-data.filter-style > form > span')
            .should('have.class', 'active');

        cy.get('.distance-from')
            .should('have.value', '')
            .type('0')
            .should('have.value', '0')
            .clear()
            .should('have.value', '');

        cy.get('.distance-to')
            .should('have.value', '')
            .click()
            .type('5000')
            .should('have.value', '5000')
            .clear()
            .should('have.value', '');
        
        cy.get('.advanced-search')
            .click();
        cy.get('#obsah > section.filter > div.filter-data.filter-style > form > span')
            .should('not.have.class', 'active');
    });

    it('keyword', () => {
        cy.get('.advanced-search')
            .click();
        cy.get('#obsah > section.filter > div.filter-data.filter-style > form > span')
            .should('have.class', 'active');

            cy.get('.keyword')
                .should('have.value', '')
                .type('Keyword')
                .should('have.value', 'Keyword')
                .clear()
                .should('have.value', '');

        cy.get('.advanced-search')
            .click();
        cy.get('#obsah > section.filter > div.filter-data.filter-style > form > span')
            .should('not.have.class', 'active');
    });
});