/// <reference types="cypress" />

import Chance from 'chance';
const chance = new Chance();

describe('Test of detail', () => {
    
    const checkIfDetailHasPhotos = $element => {
        return $element.find('.photo-preview > img').length > 0
    };
    
    beforeEach('Redirect to detail of first advertised cottage', () => {
        cy.visitChatyUrlAndAcceptCookies();

        cy.get(':nth-child(2) > .title > a')
            .click()
            .should('have.attr', 'href')
            .then((href) => {
                cy.visit(href)
            })
    })

    it('Check if show number link hide after click and if number wont be hidden after', () => {
        cy.log(chance.company());
        cy.log(chance.city());
        cy.get('.hiddenPhone')
            .contains('...');
        cy.get('.show-phone-number')
            .click();
        cy.get('.hiddenPhone')
            .should('not.contain', '...');
        cy.get('.show-phone-number')
            .should('not.exist');
    })

    it('Check if google maps are clickable and show map', () => {
        cy.get('#map_canvas-preloader > div').should('be.visible');
        cy.get('#map_canvas-preloader')
            .click();
        cy.get('#map_canvas-preloader > div').should('not.be.visible');
    });

    it('Check if back button returns from gallery', () => {
        cy.get('#content').then($content => {
            if (checkIfDetailHasPhotos($content)) {
                cy.get('.photo-preview > img')
                    .click();
                cy.go('back');
            }
        })
    });

    it('Check if exit button returns from gallery', () => {
        cy.get('#content').then($content => {
            if (checkIfDetailHasPhotos($content)) {
                cy.get('.photo-preview > img')
                    .click();
                    cy.get('#gallery-close')
                        .click();
            }
        })
    });

    it('Check if ESC returns from gallery', () => {
        cy.get('#content').then($content => {
            if (checkIfDetailHasPhotos($content)) {
                cy.get('.photo-preview > img')
                    .click();
                cy.get('body').type('{esc}');
            }
        })
    });

});
