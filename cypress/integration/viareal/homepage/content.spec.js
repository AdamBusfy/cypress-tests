/// <reference types="cypress" />
import Chance from 'chance';
const chance = new Chance();

describe('Check content', () => {
  
    beforeEach( () => {
      cy.visitViarealUrl();
    })

    it('Check if contact form works', () => {
        cy.get(':nth-child(2) > :nth-child(3) > .btn')
          .click();
        cy.get('.modal-body').should('be.visible');
        cy.get(':nth-child(1) > .form-control')
          .basicInputControlAndType(chance.email());
        cy.get(':nth-child(2) > .form-control')
          .basicInputControlAndType('0909090909');
        cy.get(':nth-child(3) > .form-control')
          .basicInputControlAndType(chance.sentence({ words: 10 }));
        cy.contains('Odoslať').click();
        cy.get('.modal-body').should('not.be.visible');
    });
  });