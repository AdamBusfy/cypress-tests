/// <reference types="cypress" />

describe('Check content', () => {
  
    beforeEach( () => {
      cy.visitNovostavbyUrlAndAcceptCookies();
    })
  
    it('Check if content is complete', () => {
      cy.get('.box-articles')
        .should('be.visible');
      cy.get('.box-articles >> .white-box')
        .should('be.visible');
    });
        
    it('Check if filter work', () => {
      cy.get('.location')
        .should('be.empty');
      cy.get('.location')
        .type('Brat');
      cy.get('.\\33 33lokalita > .fancy-selector-wrapper > .wrapper')
        .should('be.visible');
      cy.get('.\\33 33lokalita > .fancy-selector-wrapper > .wrapper')
        .contains('Bratislava')
        .click();
      cy.get('.\\32 22druh > :nth-child(1) > .select-lookalike > .select-value')
        .click();
      cy.get('.\\32 22druh > :nth-child(1) > .wrapper')
        .should('be.visible');
      cy.get('.\\32 22druh > :nth-child(1) > .wrapper')
        .contains('Byty')
        .click();
      cy.contains('Hľadať')
        .click();
      cy.get('#content > .white-box.articles > article')
        .its('length')
        .should('be.greaterThan', 0);
    });

    it('Check if detail is complete', () => {
      cy.contains('Hľadať')
        .click();
        cy.get('#content > div.span40.white-box.articles > > a.photo')
        .first()
        .invoke('removeAttr', 'target', '_blank')
        .click();
        cy.get('#content > :nth-child(9) > :nth-child(1)')
          .should('be.visible');
        cy.get(':nth-child(9) > :nth-child(2)')
          .should('be.visible');
    });
  });