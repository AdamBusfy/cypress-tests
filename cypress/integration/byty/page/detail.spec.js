/// <reference types="cypress" />

describe('Check content of detail', () => {
  
    beforeEach( () => {
      cy.visitBytyUrlAndAcceptCookies();
    })
  
    it('Check if content of detail is complete', () => {

        cy.get('#homepage-newest > div:nth-child(2) > a')
          .invoke('removeAttr', 'target', '_blank')
          .click();
        cy.get('#obsah')
          .should('exist')
          .should('be.visible');
        cy.get('#advDetailLeftBox').should('exist');
        cy.get('.advDetailRightBox').should('exist');
    });
  });