/// <reference types="cypress" />

describe('Check content', () => {
  
    beforeEach( () => {
      cy.visitBytyUrlAndAcceptCookies();
    })
  
    it('Check if content is complete', () => {
        cy.log('Check if top categories are shown');
        cy.get('#mainContent')
          .find('#topCategories >> .ico')
          .its('length')
          .should('be.greaterThan', 4);
        cy.log('Check if newest advertisements are shown');
        cy.get('#mainContent')
          .find('#homepage-newest >>> img')
          .its('length')
          .should('be.greaterThan', 5);
        cy.log('Check if articles are shown');
        cy.get('#mainContent')
          .find('#articles-housing >> .item-photo')
          .its('length')
          .should('be.greaterThan', 0);
    });

    it('Check if submit filter return results', () => {
        cy.get('[name="p[param1][from]"]')
          .basicInputControlAndType('10');
        cy.get('[name="p[param1][to]"]')
          .basicInputControlAndType('100000');
        cy.contains('Hľadať')
          .click();
        cy.contains('Vyhľadávanie bytov');
        cy.get('.advertisement-photo')
          .its('length')
          .should('be.greaterThan', 0);
    });
    
    
  });