/// <reference types="cypress" />

import Chance from 'chance';
const chance = new Chance();

describe('Detail overview', () => {
    
    beforeEach(() => {
        cy.visitNehnutelnostiUrlAndAcceptCookies();

        cy.contains('Mohlo by vás zaujímať')
            .scrollIntoView();
            
        cy.get('.active > a > figure > .item-preview--about > .item-preview--title')
            .click();
    });

    it('Detail content should be visible', () => {
        cy.get('#photo-gallery-preview')
            .should('be.visible');
        cy.get('.box-aside--contact-form > .d-flex')
            .should('contain', 'Odoslať správu');
        cy.get('.mb-2 > .mr-3')
            .should('be.visible');
        cy.get('.top--info-location')
            .should('be.visible');
        cy.get('.price--main > span')
            .should('be.visible');
        cy.contains('Akcie inzerátu')
            .should('be.visible');
        cy.get('#map-detail')
            .should('be.visible');
        cy.contains('Kontakt na predajcu')
            .should('be.visible');
    });

    it('Gallery view back button', () => {
        cy.get('#photo-gallery-preview > img')
            .click();
        cy.get('.lg-close')
            .click();
        cy.get('#photo-gallery-preview')
            .should('be.visible');
    });

    it('Show mobile number', () => {
        cy.get('.contact-content > .show-number > .img-placeholder > div > strong')
            .should('be.visible');
        cy.get('.contact-content > .show-number > .img-placeholder > div > strong')
            .click();
        cy.get('.contact-content > .show-number > .img-placeholder > div > strong')
            .should('not.be.visible');
    });

    // it('Redirect to all advertisments of seller from detail', () => {
    //     cy.get('.info--contact-name > .link')
    //         .click();
    //     cy.get('[data-real-estate-menu-item="inzeráty"]')
    //         .click();
    //     cy.url().should('contain', '/inzeraty')
    // });

    it('Map modal after clicking on map', () => {
        cy.get('.modal-map--detail > .modal-dialog > .modal-content > .modal-body')
            .should('not.be.visible');
        cy.get('.label')
            .contains('Mapa')
            .click();
        cy.get('.modal-map--detail > .modal-dialog > .modal-content > .modal-body')
            .should('not.be.visible');

        cy.get('.ol-zoom-in').click({waitForAnimations:true});
        cy.get('.ol-zoom-out').click({waitForAnimations:true});

        cy.get('.modal-map--detail > .modal-dialog > .modal-content > .modal-body > .close-button > span > img')
            .click();
        
        cy.get('.modal-map--detail > .modal-dialog > .modal-content > .modal-body')
            .should('not.be.visible');
    });

    it('Send email to seller', () => {
        cy.get('.container > .row > :nth-child(1) > label.w-100 > .placeholder')
            .basicInputControlAndType(chance.name());
        cy.get('.container > .row > :nth-child(2) > label.w-100 > .placeholder')
            .basicInputControlAndType(chance.email());
        cy.get('.container > .row > :nth-child(3) > label.w-100 > .placeholder')
            .basicInputControlAndType('0909090909');
    });

    it('Arrange a meeting', () => {
        cy.get('.ask-for-inspection-modal > .modal-dialog > .modal-content > .modal-body')
            .should('not.be.visible');
        cy.get('.button--primary-invert')
            .contains('Dohodnúť obhliadku')
            .click();
        cy.get('.ask-for-inspection-modal > .modal-dialog > .modal-content > .modal-body')
            .should('be.visible');
        cy.get(':nth-child(1) > .form-group > .form-control')
            .basicInputControlAndType(chance.phone());
        cy.get(':nth-child(2) > .form-group > .form-control')
            .basicInputControlAndType(chance.email());
        cy.get('.ask-for-inspection-modal > .modal-dialog > .modal-content > .modal-body > .close-button')
            .click();
        cy.get('.ask-for-inspection-modal > .modal-dialog > .modal-content > .modal-body')
            .should('not.be.visible');
    });
});