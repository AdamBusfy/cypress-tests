/// <reference types="cypress" />


var i = 0;
for (i = 0; i < 1 ; i++) { 

describe('Homepage filter inputs', () => {
  beforeEach( () => {
    cy.visitNehnutelnostiUrlAndAcceptCookies();
  })

  it('Input location filter - wrapper function, checkboxes and submit function', () => {
    cy.get('.border-box')
      .should('not.contain', 'Slovensko');
    cy.get('.input-placeholder')
      .should('be.empty')
      .and('be.enabled')
      .click();
    cy.get('.border-box')
      .should('contain', 'Slovensko');

    cy.get('.border-box')
      .should('contain', 'Banskobystrický kraj');
    cy.get(':nth-child(2) > :nth-child(1) > :nth-child(1) > [data-collapse-wrapper="true"] > .lt-app-box-tree-collapse-button')
      .click();
    cy.get('.border-box')
      .should('contain', 'Detva');
    cy.get(':nth-child(2) > :nth-child(1) > :nth-child(1) > [data-collapse-wrapper="true"] > .lt-app-box-tree-collapse-button')
      .click();
    cy.get('.border-box')
      .should('not.contain', 'Detva');
    cy.get('.lt-box-tree-tree > :nth-child(1) > :nth-child(1) > [data-collapse-wrapper="true"] > .lt-app-box-tree-collapse-button')
      .click();
    cy.get('.border-box')
      .should('not.contain', 'Banskobystrický kraj');
    cy.get(':nth-child(3) > :nth-child(1) > [data-collapse-wrapper="true"] > .lt-app-box-tree-collapse-button')
      .click();
    cy.get('.border-box')
      .should('contain', 'Belgicko');
    cy.get(':nth-child(3) > :nth-child(1) > [data-collapse-wrapper="true"] > .lt-app-box-tree-collapse-button')
      .click();
    cy.get('.border-box')
      .should('not.contain', 'Belgicko');

    cy.get('#lt-box-tree-node-checkbox-c1')
      .should('not.be.checked');
    cy.get('.lt-box-tree-tree > :nth-child(1) > :nth-child(1) > label')
      .click();
    cy.get('#lt-box-tree-node-checkbox-c1')
      .should('be.checked');

    cy.get('#lt-box-tree-node-checkbox-c2')
      .should('not.be.checked');
    cy.get('.lt-box-tree-tree > :nth-child(2) > :nth-child(1) > label')
      .click();
    cy.get('#lt-box-tree-node-checkbox-c2')
      .should('be.checked');

    cy.get('#lt-box-tree-node-checkbox-c12')
      .should('not.be.checked')
    cy.get('.lt-box-tree-tree > :nth-child(3) > :nth-child(1) > label')
      .click();
    cy.get('#lt-box-tree-node-checkbox-c12')
      .should('be.checked')

    cy.get('.lt-box-button-clear')
      .click()
      .then(() => {
        cy.get('#lt-box-tree-node-checkbox-c1')
          .should('not.be.checked');

        cy.get('#lt-box-tree-node-checkbox-c2')
          .should('not.be.checked');

        cy.get('#lt-box-tree-node-checkbox-c12')
          .should('not.be.checked');
      })

    cy.get('.lt-box-button-submit')
      .click()
      .then(() => {
        cy.get('.border-box')
          .should('not.contain', 'Slovensko');
      });

    cy.get('.lt-input-input')
      .click();

    cy.get(':nth-child(2) > :nth-child(1) > :nth-child(1) > [data-collapse-wrapper="true"] > .lt-app-box-tree-collapse-button')
      .click();
    cy.get('.lt-box-tree-tree > :nth-child(1) > :nth-child(1) > label')
      .click()
      .then(() => {
        cy.get('#lt-box-tree-node-checkbox-d1')
          .should('be.checked');
        cy.get('#lt-box-tree-node-checkbox-d1')
          .should('be.checked');
        cy.get('#lt-box-tree-node-checkbox-r2')
          .should('be.checked');

        cy.get('.lt-box-selected-list > ul > li')
          .its('length')
          .should('eq', 1);
      });
    cy.get('.lt-box-tree-tree > :nth-child(1) > :nth-child(1) > label')
      .click()
      .then(() => {
        cy.get('#lt-box-tree-node-checkbox-d1')
          .should('not.be.checked');
        cy.get('#lt-box-tree-node-checkbox-d1')
          .should('not.be.checked');
        cy.get('#lt-box-tree-node-checkbox-r2')
          .should('not.be.checked');
        cy.get('.lt-box-selected-list > ul > li').should('not.exist');
      });

    cy.get('.lt-box-tree-tree > :nth-child(1) > :nth-child(1) > label')
      .click();

    cy.get('.lt-box-tree-tree > :nth-child(1) > :nth-child(2) > :nth-child(1) > :nth-child(1) > label')
      .click()
      .then(() => {
        cy.get('#lt-box-tree-node-checkbox-r1')
          .should('not.be.checked');
        cy.get('.lt-box-selected-list > ul > li')
          .its('length')
          .should('eq', 7);
      });

    cy.get('.lt-box-tree-tree > :nth-child(1) > :nth-child(2) > :nth-child(1) > :nth-child(1) > label')
      .click()
      .then(() => {
        cy.get('.lt-box-selected-list > ul > li')
          .its('length')
          .should('eq', 1);
      });

    cy.get(':nth-child(1) > :nth-child(2) > :nth-child(1) > :nth-child(2) > :nth-child(1) > :nth-child(1) > label')
      .click()
      .then(() => {
        cy.get('#lt-box-tree-node-checkbox-d1')
          .should('not.be.checked');
          cy.get('.lt-box-selected-list > ul > li')
          .its('length')
          .should('be.greaterThan', 7);
      });

    cy.get(':nth-child(1) > :nth-child(2) > :nth-child(1) > :nth-child(2) > :nth-child(1) > :nth-child(1) > label')
    .click()
    .then(() => {
      cy.get('#lt-box-tree-node-checkbox-d1')
        .should('be.checked');
        cy.get('.lt-box-selected-list > ul > li')
        .its('length')
        .should('eq', 1);
    });

    cy.get('.lt-box-selected-item > button')
      .click()
      .then(() => {
        cy.get('.lt-box-selected-list > ul > li')
        .should('not.exist');
        
        cy.get('#lt-box-tree-node-checkbox-c1')
          .should('not.be.checked');
      });
      cy.get('.lt-box-button-submit')
        .click();
      
      cy.get('.lt-box-box')
        .should('not.exist');

      cy.get('.lt-input-input')
        .type('Slovensko')
        .then(() => {
          cy.get('.lt-box-box')
            .should('exist')
            .and('contain', 'Slovensko');
          
          cy.get('.lt-input-input')
            .should('have.value', 'Slovensko')
            .clear()
            .should('be.empty');
        });
  });

  it('Type filter', () => {
    cy.get('.col-xl-3.order-xl-3 > .form-group > .form-control')
    .should('have.value', '')
    .select('24')
    .should('have.value', '24');
  });

  // it('Video checkbox', () => {

  //   cy.get('#filter-form--has-video')
  //   .should('not.be.checked');

  //   cy.get('.form-control--checkbox')
  //     .click()
  //     .then(() => {
  //       cy.get('#filter-form--has-video')
  //         .uncheck({force:true})
  //         .should('be.checked');
  //     });
      
  //   cy.get('.form-control--checkbox')
  //     .click()
  //     .then(() => {
  //       cy.get('#filter-form--has-video')
  //         .uncheck({force:true})
  //         .should('not.be.checked');
  //     });
  //   });

  it('Category filter', function() {
    cy.get('.border-box')
      .should('not.contain', 'Garsónka');
    cy.get('#filter-form__categories--select-box')
      .click();
    cy.get('.border-box')
      .should('contain', 'Garsónka');

    cy.get('.all-categories-label > .fakeCheckbox')
      .click();
    cy.get('[data-filter-main-category-name="Byty"] > .link')
      .click();
      cy.get('.all-categories-label > .fakeCheckbox')
      .click();
    cy.get('[data-filter-main-category-name="Byty"] > .link')
      .click();
    cy.get('.button-bg > :nth-child(3)')
      .click();
    });

    it('Advanced searching', () => {

      cy.get('.filter-additional-collapse > :nth-child(1) > :nth-child(1) > .form-group > .label')
      .should('not.be.visible');

      cy.get('.order-xl-1 > .form-group > .label')
        .click()
        .then(() => {
          cy.contains('Rozšírené hľadanie')
            .click();
        });

      cy.get('.filter-additional-collapse > :nth-child(1) > :nth-child(1) > .form-group > .label')
      .should('be.visible');

      cy.contains('Menej možností')
            .click();

      cy.get('.filter-additional-collapse > :nth-child(1) > :nth-child(1) > .form-group > .label')
        .should('not.be.visible');
    });

    it('Condition filter', () => {
      cy.get('.order-xl-1 > .form-group > .label')
        .click()
        .then(() => {
          cy.contains('Rozšírené hľadanie')
            .click();
        });

      cy.get('.filter-additional-collapse > :nth-child(1) > :nth-child(1) > .form-group > .form-control')
        .select('12')
        .should('have.value', '12');
    });

    it('Price from -> to', () => {
      cy.get('.order-xl-1 > .form-group > .label')
        .click()
        .then(() => {
          cy.contains('Rozšírené hľadanie')
            .click();
        });

      cy.get(':nth-child(2) > .row > .pr-0 > .form-group > .form-control')
        .should('be.empty')
        .type('100')
        .should('have.value', '100')
        .clear()
        .should('be.empty');

      cy.get(':nth-child(2) > .row > .pl-0 > .form-group > .form-control')
        .should('be.empty')
        .type('50000')
        .should('have.value', '50000')
        .clear()
        .should('be.empty');

    });

    it('Distance from -> to', () => {
      cy.get('.order-xl-1 > .form-group > .label')
        .click()
        .then(() => {
          cy.contains('Rozšírené hľadanie')
            .click();
        });

      cy.get(':nth-child(2) > .row > .pr-0 > .form-group > .label')
          .should('be.visible');
      
      cy.get('[name="p[param1][from]"]')
        .should('be.empty')
        .type('100')
        .should('have.value', '100')
        .clear()
        .should('be.empty');
        
        cy.get(':nth-child(2) > .row > .pr-0 > .form-group > .label')
          .should('be.visible');
          
      cy.get('[name="p[param1][to]"]')
        .should('be.empty')
        .type('50000')
        .should('have.value', '50000')
        .clear()
        .should('be.empty');
    });

    it('Keyword input filter', () => {
      cy.get('.order-xl-1 > .form-group > .label')
        .click()
        .then(() => {
          cy.contains('Rozšírené hľadanie')
            .click();
        });
      
      cy.get(':nth-child(2) > .row > .pr-0 > .form-group > .label')
        .should('be.visible');

      cy.get(':nth-child(4) > .form-group > .form-control')
        .should('be.empty')
        .type('Keyword')
        .should('have.value', 'Keyword')
        .clear()
        .should('be.empty');
    });
    
})};