/// <reference types="cypress" />


describe('Submit search with parameters, check redirect to result page', () => {

    const checkIfContentExists = () => {
        cy.get('.advertisement-item--content__title')
            .its('length')
            .should('be.greaterThan', 5);
        cy.get('.advertisement-item--content__text')
            .its('length')
            .should('be.greaterThan', 5);
        cy.get('.advertisement-item--content__price')
            .its('length')
            .should('be.greaterThan', 5);    
    };

    const fillAndSubmitFilter = () => {
        // fill location filter

        cy.get('.border-box')
            .should('not.contain', 'Slovensko');
        cy.get('.input-placeholder')
            .should('be.empty')
            .and('be.enabled')
            .click();
        cy.get('.border-box')
            .should('contain', 'Slovensko');
        
        cy.get('#lt-box-tree-node-checkbox-c1')
            .should('not.be.checked');
        cy.get('.lt-box-tree-tree > :nth-child(1) > :nth-child(1) > label')
            .click();
        cy.get('#lt-box-tree-node-checkbox-c1')
            .should('be.checked');
        cy.get('.lt-box-button-submit')
            .click();

         // fill type filter

        cy.get('.col-xl-3.order-xl-3 > .form-group > .form-control')
            .should('have.value', '')
            .select('24')
            .should('have.value', '24');

        cy.contains('Hľadať')
            .click();  
    };

    before(() => {
        cy.visitNehnutelnostiUrlAndAcceptCookies();
    })

    it('Check if redirect was successfull', () => {
        fillAndSubmitFilter()
        cy.contains('Predaj nehnuteľností Slovensko');
        checkIfContentExists();
    })

    it('Redirect to second result page ', () => {
        cy.get('.component-pagination__items > :nth-child(2) > a')
            .scrollIntoView()
            .click();

        checkIfContentExists();
    });
    
    it('Editing submited filter', () => {

        cy.get('#filter-form__categories--select-box')
        .click();
        cy.get('.border-box')
        .should('contain', 'Garsónka');

        cy.get('[data-filter-main-category-name="Byty"] > .bold > .fakeCheckbox')
            .click();
    
        cy.get('.button-bg > :nth-child(3)')
            .click();

        cy.contains('Hľadať')
            .click();
            
        checkIfContentExists();
    });

    it('Save, create and delete agent created from search', () => {
        cy.contains('Uložiť hľadanie')
            .click();
        cy.contains('Váš e-mail')
            .type('test@test.com');
        cy.contains('Názov hľadania')
            .children('input')
            .clear()
            .type('Moj novy agent');
        cy.get('#send-frequency')
            .select('2');
        cy.get('.d-flex.text-left > .s_repbox > .px-4 > .modal-dialog > .modal-content > .modal-body > .d-block > .order-1 > .submit-button')
            .click();
        cy.contains('Agent uložený');
        cy.get('.show > .modal-dialog > .modal-content > .modal-body > :nth-child(5) > .modal-link')
            .click();
        cy.contains('Moj novy agent');
        cy.contains('Zmazať')
            .click();
        cy.get('.btn_confirm')
            .click();
        cy.contains('Nemáte žiadne hľadanie'); 
    });
})