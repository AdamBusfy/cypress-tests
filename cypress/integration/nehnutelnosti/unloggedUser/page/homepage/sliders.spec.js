/// <reference types="cypress" />

describe('Homepage sliders', () => {
    beforeEach( () => {
      cy.visitNehnutelnostiUrlAndAcceptCookies();
    })

    it('Top slider - viewport 1024:768', () => {
        cy.viewport(1024, 768);

        cy.get('.top-offers-heading')
          .scrollIntoView();

        //vytiahne pocet obrazkov v hornom slideri pri danom rozliseni a overi ci sa nacital dostatocny pocet
        cy.get('.background--red > .container')
            .find('a > figure .dev-photo-preview-box > img')
            .its('length')
            .should('be.greaterThan', 5);

        cy.get('.background--red > .container')
            .find('.item-preview--title')
            .its('length')
            .should('be.greaterThan', 5);

        cy.get('.background--red > .container')
            .find('.item-preview--desc')
            .its('length')
            .should('be.greaterThan', 5);
    })

    it('Top slider - viewport 320:568', () => {
        cy.viewport(320, 568);

        cy.get('.top-offers-heading')
          .scrollIntoView();
          
        //vytiahne pocet obrazkov v hornom slideri pri danom rozliseni a overi ci sa nacital dostatocny pocet
        cy.get('.background--red > .container')
          .find('a > figure .dev-photo-preview-box > img')
          .its('length')
          .should('be.greaterThan', 2);

        cy.get('.background--red > .container')
          .find('.item-preview--title')
          .its('length')
          .should('be.greaterThan', 2);
        
        cy.get('.background--red > .container')
          .find('.item-preview--desc')
          .its('length')
          .should('be.greaterThan', 2);

    })
})