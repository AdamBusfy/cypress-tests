/// <reference types="cypress" />


describe('Create, edit and delete search agent', () => {

    before(() => {
        cy.visitNehnutelnostiUrlAndAcceptCookies();
        cy.visit('https://www.nehnutelnosti.sk/moje-hladanie/');
    })

    it('Create. edit and delete search agent', () => {
        cy.log('Creating new search agent');
        cy.contains('Vytvoriť hľadanie')
            .click();
        cy.get('#param3')
            .should('have.value', '')
            .select('24')
            .should('have.value', '24');
        cy.get('#cenaOd')
          .basicInputControlAndType('0');
        cy.get('#cenaDo')
          .basicInputControlAndType('30000');
        cy.get(':nth-child(6) > :nth-child(1) > .form-control')
          .basicInputControlAndType('test');
        cy.get(':nth-child(6) > :nth-child(2) > .form-control')
          .basicInputControlAndType('test@test.test');
        cy.get('#reporterFrequency')
          .should('have.value', '0')
          .select('2')
          .should('have.value', '2');
        cy.get('.button-secondary')
          .click();
        cy.url({timeout:15000})
          .should('eq', 'https://www.nehnutelnosti.sk/moje-hladania/');

        cy.log('Editing search agent');
        cy.contains('Upraviť')
          .first()
          .click();
        cy.get('#vymeraOd')
          .basicInputControlAndType('10');
        cy.get('#vymeraDo')
          .basicInputControlAndType('1000');
        cy.get(':nth-child(6) > :nth-child(1) > .form-control')
          .clear()
          .basicInputControlAndType('EDITED');
        cy.get('#reporterFrequency')
          .should('have.value', '2')
          .select('3')
          .should('have.value', '3');
        cy.get('.button-secondary')
          .click();
        cy.contains('EDITED');
        
        cy.log('Deleting search agent');
        cy.contains('Zmazať')
          .first()
          .click();
        cy.get('.btn_confirm')
          .click();
        cy.should('not.exist', 'EDITED');
    });
    
})