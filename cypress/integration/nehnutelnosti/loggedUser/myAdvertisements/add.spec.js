/// <reference types="cypress" />

import Chance from 'chance';
const chance = new Chance();

describe('Add new advertisement as an logged user', () => {
    
    before(() => {
        cy.viewport(1240, 1000);
        cy.visitNehnutelnostiAndLogin()
        cy.get('#dropdownMenuButton2')
        .click();
        cy.get('[data-ga-action="pridat-inzerat"]')
        .click();
    });

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('AZetSecId')
    });

    it('Photo upload', () => {
        cy.get('#uploadPhotoCard')
            .should('not.be.visible');
        cy.get('#photoCountProgress')
            .should('not.have.class', 'photoCountProgressStatic');

        const fixtureFile = 'img/test.png';
        cy.get('#photo-container input[type="file"]').attachFile(fixtureFile);

        // cy.get('#photo-container input[type="file"]').attachFile(fixtureFile, {subjectType: 'drag-n-drop'});
        
        cy.get('#uploadPhotoCard')
            .should('be.visible');
      
    });

    it('Type input', () => {
        cy.get('#param3')
            .should('have.value', 0)
            .select('24')
            .should('have.value', 24);
        // cy.get('#param12')
    });

    it('Category input', () => {
        cy.get('.fieldsetWithLocation > .form > :nth-child(2) > :nth-child(2)')
            .find('select')
            .should('have.value', 0)
            .select('10000')
            .should('have.value', 10000)
    });

    it('Title input', () => {
        cy.get('#nadpis').basicInputControlAndType(chance.company());   
    });

    it('Text input', () => {
        cy.get('#text').basicInputControlAndType(chance.sentence());
    });

    it('Useful area input', () => {
        cy.get('#param4').basicInputControlAndType(0);
    });

    it('Built-up area', () => {
        cy.get('#param5').basicInputControlAndType(1000);
    });

    it('Area input', () => {
        cy.get('#param6').basicInputControlAndType(1500);
    });

    it('Actual condition input', () => {
        cy.get('#param7')
            .should('have.value', 0)
            .select('12')
            .should('have.value', 12); 
    });

    it('District, city, street, description number inputs', () => {
        cy.contains('Nájsť na mape')
            .should('not.be.visible');

        cy.get('.cityPicker')
            .should('be.disabled');

        cy.get('#data\\[locationPicker\\]')
            .should('have.value', 'c0')
            .select('c1r1d1')
            .should('have.value', 'c1r1d1');

        cy.get('.cityPicker')
            .should('be.enabled');
        
        cy.get('#param8-street-form > div.row > div:nth-child(1) > div:nth-child(3) > input')
            .should('be.visible');
        cy.get('#param8-street-form > div.row > div:nth-child(1) > div:nth-child(2) > select')
            .should('not.be.visible');
        
        cy.get('.cityPicker')
            .select('Brusno');

        cy.get('#param8-street-form > div.row > div:nth-child(1) > div:nth-child(2) > select')
            .should('be.visible');
        cy.get('#param8-street-form > div.row > div:nth-child(1) > div:nth-child(3) > input')
            .should('not.be.visible');

        cy.get('#param8-street-form > .row > :nth-child(2) >> input ')
            .should('be.visible');
        cy.get('#param8-street-form > .row > :nth-child(2) >> select ')
            .should('not.be.visible');


        cy.get('#param8-street-form > div.row > div:nth-child(1) > div:nth-child(2) > select')
            .select('Brusenec', {force:true});

        cy.get('#param8-street-form > .row > :nth-child(2) >> input ')
            .should('not.be.visible');
        cy.get('#param8-street-form > .row > :nth-child(2) >> select ')
            .should('be.visible');

        cy.contains('Nájsť na mape')
            .should('be.visible');
    });

    it('Price from - to', () => {
        cy.get('#param1')
            .basicInputControlAndType(1000000);

        cy.get('#param12')
            .select('6')    
            .should('have.value', '6');
    });

    it('Energies input', () => {
        cy.get('#param20')
            .basicInputControlAndType(1000);
    });
    it('Name already filled', () => {
        cy.get('#meno')
            .contains('testovaciUcetZTS');
    });

    it('Email already filled', () => {
        cy.get('#emailContact')
            .should('have.value','testovaciUcetZTS@azet.sk');
    });

    it('Phone already filled', () => {
        cy.get('#phoneContact')
            .should('have.value','0909090909');
    });

    it('Terms check', () => {

        cy.get('#podmienky > label ').click('left');
        cy.get('#agreement').should('be.checked');
        
    });

    after('Submit form', () => {
        cy.get('#pokracovat2')
            .click();
        cy.contains('Inzerát ste úspešne vytvorili', {timeout:25000});
        cy.get('.menu--dropdown').click();
        cy.get('#auth-buttons-dropdown-parent > #dropdownMenuButton').click();
        cy.get('#ab_logout').click();
    });

});