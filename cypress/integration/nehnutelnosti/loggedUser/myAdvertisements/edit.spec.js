/// <reference types="cypress" />

import Chance from 'chance';
const chance = new Chance();

describe('Edit advertisement as an logged user', () => {
    
    before(() => {
        cy.viewport(1240, 1000);
        cy.visitNehnutelnostiAndLogin()
        cy.get('#dropdownMenuButton2')
          .click();
        cy.get('[data-ga-action="moje-inzeraty"]')
          .click();
    });

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('AZetSecId')
    });

    it('Open all advertisement and edit first', () => {
        cy.get(':nth-child(1) > a > .t')
          .click();
        cy.get('.my-advertisement__options--btn')
          .first()
          .click();
        cy.get('.updateItem')
          .first()
          .click();
        cy.get('#nadpis')
          .clear()
          .type('EDITED');
          cy.get('#pokracovat2')
            .click();
        cy.contains('Inzerát ste úspešne upravili', {timeout:250000});
        cy.get('#auth-buttons-dropdown-parent > #dropdownMenuButton').click();
        cy.get('#ab_logout').click();
    });


});