// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

import 'cypress-file-upload';

Cypress.Commands.add('visitViarealUrl', () => {
    // cy.clearCookies();
    // cy.visit(Cypress.env('viarealUrl'), {
    //     onBeforeLoad: (win) => {
    //         win.localStorage.clear()
    //         win.sessionStorage.clear();        }
    //   })
    cy.visit(Cypress.env('viarealUrl'));
    // cy.get('.fc-cta-consent').click();
});

Cypress.Commands.add('visitChatyUrlAndAcceptCookies', () => {
    // cy.clearCookies();

    // cy.visit(Cypress.env('chatyUrl'), {
    //     onBeforeLoad: (win) => {
    //         win.localStorage.clear()
    //         win.sessionStorage.clear();        
    //     }
    //   })
    cy.visit(Cypress.env('chatyUrl'));
    // cy.get('.fc-cta-consent').click();
});

Cypress.Commands.add('visitBytyUrlAndAcceptCookies', () => {
    // cy.clearCookies();

    // cy.visit(Cypress.env('bytyUrl'), {
    //     onBeforeLoad: (win) => {
    //         win.localStorage.clear()
    //         win.sessionStorage.clear();       
    //      }
    //   })
    cy.visit(Cypress.env('bytyUrl'));
    // cy.get('.fc-cta-consent').click();
});

Cypress.Commands.add('visitNovostavbyUrlAndAcceptCookies', () => {
    // cy.clearCookies();

    // cy.visit(Cypress.env('novostavbyUrl'), {
    //     onBeforeLoad: (win) => {
    //         win.localStorage.clear()
    //         win.sessionStorage.clear();
    //     }
    //   })
    cy.visit(Cypress.env('novostavbyUrl'));
    // cy.get('.fc-cta-consent').click();
});

Cypress.Commands.add('visitNehnutelnostiUrlAndAcceptCookies', () => {
    // cy.clearCookies();

    // cy.visit(Cypress.env('nehnutelnostiUrl'), {
    //     onBeforeLoad: (win) => {
    //         win.localStorage.clear()

    //         win.sessionStorage.clear();
    //     }
    //   })
    cy.visit(Cypress.env('nehnutelnostiUrl'));
    // cy.get('.close')
    //   .click();
})

Cypress.Commands.add('visitNehnutelnostiAndLogin', () => {
    // cy.clearCookies();

    // cy.visit(Cypress.env('nehnutelnostiUrl'), {
    //     onBeforeLoad: (win) => {
    //         win.localStorage.clear()

    //       win.sessionStorage.clear();
    //     }
    //   })

    cy.visit(Cypress.env('nehnutelnostiUrl'));

    cy.request({
        form: true,
        method: "POST",
        url: "https://prihlasenie.azet.sk/remote-login?project=azet&uri=https%3A%2F%2Fwww.nehnutelnosti.sk%2F",
        body: {
            'form[username]': 'testovaciucetZTS',
            'form[password]': 'testovaciehesloZTS'
        }
      });

      cy.reload();

})

/**
     * Check if input is empty, then type in label
     * and check if input contains label
     *
     */
Cypress.Commands.add("basicInputControlAndType", { prevSubject: 'element'}, (subject, label) => { 
    cy.get(subject)
    .should('be.empty')
    .type(label)
    .should('have.value', label);
})

